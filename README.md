# TNO Security Gateway - Plugfest Q2 2022 Hands-On

This repository contains the necessary configuration and documentation to deploy the TSG Core Container.

With the steps in this readme an artifact with an attached policy can be retrieved from a IDS connector.

There are three different deployment scenarios, choose the one that fits your environment:
- _Docker compose_: for deployments on local machines with Docker installed.
- _Local Kubernetes_: for deployments on local machines with a Kubernetes cluster.
- _Public Kubernetes_: for deployments on cloud infrastructure with a Kubernetes cluster.

## Configuration

For the default local deployments no modifications are required.

There are, however, some settings that can be modified for this workshop. The configuration files are either `application.yaml` (_Docker-compose_), `values.local.yaml` (_Local Kubernetes_), `values.public.yaml` (_Public Kubernetes_):

* **_Change IDS Identity_** (only if you have an existing IDS identity)  
  The IDS identity can be changed from the demo certificate to a certificate that is registered in either the SCSN DAPS or the AISEC DAPS.  
  * Replace the files: `cachain.crt`, `component.crt`, `component.key` with the PEM-based CA chain, public and private keys
  * Modify `daps.url` to the corresponding DAPS URL for this identity
  * Modify `info.idsid`, `info.curator`, `info.maintainer` to the corresponding identifiers
* **_Change Admin User credentials_**  
  The default password is `plugfest` but this can be changed to a more secure password, which is especially needed for public Kubernetes deployments.
  * Modify `security.users[0].password` to a new BCrypt encoded password (e.g. via https://bcrypt-generator.com/)
* **_Make the connector accessible for others_** (only for public Kubernetes deployments)  
  To make the connector available for other connectors Ingresses are used.
  * Modify `host` to the domain name you configured with the ingress controller
  * (Optionally) Set `useNewIngress` to `false` in case you're using an older version of Kubernetes (<=v1.21)
  * (Optionally) Modify `coreContainer.ingress.clusterIssuer`, `coreContainer.ingress.secretName`, and/or `coreContainer.ingress.annotations` to change the behaviour of the main Ingress
  * (Optionally) Modify `adminUi.ingress.clusterIssuer`, `adminUi.ingress.secretName`, and/or `adminUi.ingress.annotations` to change the behaviour of the admin UI Ingress

## Docker Compose

The Docker compose deployment is the easiest way of deploying the TSG Core Container, since it can be deployed on a local machine. The drawback of using Docker compose is that for more productive Kubernetes is often a better choice for maintainability.

The requirements for this deployment are rather straightforward. Only a [Docker daemon](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/) are required. The easiest way of installing this is by using [Docker Desktop](https://docs.docker.com/desktop/).

To deploy the TSG Core Container execute:

```bash
docker-compose up -d
```

The containers will be deployed in detached mode, the user interface of the TSG Core Container will be available at: http://localhost:31000

To follow the logs of the TSG Core Container execute:

```bash
docker-compose logs -f tsg-core-container
```

To clean-up all resources of the hands-on execute:

```bash
docker-compose down
```

## Local Kubernetes (Helm)

The local Kubernetes deployment provides an rather easy way of deploying the TSG Core Container. It requires more setup than the Docker-compose variant, but the deployment itself isn't complicated.

The requirements for the local Kubernetes deployment are:
- Kubernetes Cluster (e.g., via [Docker Desktop](https://docs.docker.com/desktop/kubernetes/), [Minikube](https://minikube.sigs.k8s.io/docs/), [K3s](https://k3s.io/), [MicroK8s](https://microk8s.io/))
- [kubectl](https://kubernetes.io/docs/tasks/tools/): Kubernetes CLI
- [Helm](https://helm.sh/docs/intro/install/): Package manager for Kubernetes
- [Lens](https://k8slens.dev/) (optional, but recommended): Graphical user interface for Kubernetes

Verify that the required parts are working, execute:
```bash
kubectl get pods
helm version
```
Which should output:
```
No resources found in default namespace.
version.BuildInfo{Version:"v3.8.2", GitCommit:"6e3701edea09e5d55a8ca2aae03a68917630e91b", GitTreeState:"clean", GoVersion:"go1.18.1"}
```

When everyting is setup correctly the deployment of the TSG Core Container can be executed. First, the IDS Identity certificate must be stored as secret within the cluster:

```bash
kubectl create secret generic ids-identity-secret --from-file=ids.crt=./component.crt --from-file=ids.key=./component.key --from-file=ca.crt=./cachain.crt
```

Secondly, the TSG Core Container deployment can be created. For this the Helm repository must be added and the deployment installed:

```bash
helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm
helm repo update
helm upgrade --install plugfest tsg/tsg-connector --version 3.0.0-master -f values.local.yaml
```

The user interface of the TSG Core Container will be available at: http://localhost:31000

To view the deployment and show the logs of the TSG Core Container, either _Lens_ or _kubectl_ can be used.
For Lens, the main view will be _Workloads > Pods_. For kubectl the following commands can be used to view information of the deployment:
- `kubectl get pods`
- `kubectl logs -f deployment/plugfest-tsg-connector-core-container`

To remove all of the resources execute:
```bash
helm uninstall plugfest
kubectl delete secret/ids-identity-secret
```

## Public Kubernetes (Helm)

Edit values.yaml

The public Kubernetes deployment provides the most production-ready environment. This does however require more prerequisites to be in place. 

The requirements for the local Kubernetes deployment are:
- Publicly available Kubernetes Cluster (e.g., via [Azure AKS](https://docs.microsoft.com/en-us/azure/aks/), [Amazon EKS](https://aws.amazon.com/eks/), [Google GKE](https://cloud.google.com/kubernetes-engine), [OVHCloud](https://www.ovhcloud.com/en-ie/public-cloud/kubernetes/))
    - Domain name with control over DNS
    - Nginx Ingress Controller (make sure your DNS points towards the Nginx Ingress Controller)
    - Cert Manager
- [kubectl](https://kubernetes.io/docs/tasks/tools/): Kubernetes CLI
- [Helm](https://helm.sh/docs/intro/install/): Package manager for Kubernetes
- [Lens](https://k8slens.dev/) (optional, but recommended): Graphical user interface for Kubernetes

Verify that the required parts are working, execute:
```bash
kubectl get pods
helm version
```
Which should output:
```
No resources found in default namespace.
version.BuildInfo{Version:"v3.8.2", GitCommit:"6e3701edea09e5d55a8ca2aae03a68917630e91b", GitTreeState:"clean", GoVersion:"go1.18.1"}
```
Also the ingress controller should be reachable, when you open the public IP address of the ingress controller you should see a default message.

When everyting is setup correctly the deployment of the TSG Core Container can be executed. First, the IDS Identity certificate must be stored as secret within the cluster:

```bash
kubectl create secret generic ids-identity-secret --from-file=ids.crt=./component.crt --from-file=ids.key=./component.key --from-file=ca.crt=./cachain.crt
```

Secondly, the TSG Core Container deployment can be created. For this the Helm repository must be added and the deployment installed:

```bash
helm repo add tsg https://nexus.dataspac.es/repository/tsg-helm
helm repo update
helm upgrade --install plugfest tsg/tsg-connector --version 3.0.0-master -f values.public.yaml
```

The user interface of the TSG Core Container will be available at: https://domain.name/ui/

To view the deployment and show the logs of the TSG Core Container, either _Lens_ or _kubectl_ can be used.
For Lens, the main view will be _Workloads > Pods_. For kubectl the following commands can be used to view information of the deployment:
- `kubectl get pods`
- `kubectl logs -f deployment/plugfest-tsg-connector-core-container`

To remove all of the resources execute:
```bash
helm uninstall plugfest
kubectl delete secret/ids-identity-secret
```

## Consuming an artifact

An artifact is provided by a connector deployed on https://plugfest.beta.ids.smart-connected.nl. The following steps show how this artifact can be retrieved from this connector.

The steps that must be followed are:
1. Retrieve metadata of the artifact
2. Perform the contract negotiation process to establish a contract agreement
3. Retrieve the artifact

### 1. Retrieve metadata of the artifact

Navigate to http://localhost:31000/#/selfdescription/request (or https://domain.name/ui/#/selfdescription/request)

Fill in the form with the following values:
- _Connector ID_: `urn:scsn:ids:Plugfest-2022-Q2-Provider`
- _Agent ID_: leave empty
- _Access URL_: `https://plugfest.beta.ids.smart-connected.nl/selfdescription`

And click `Request description`.

![](screenshots/request-self-description.png)

When the self description of the connector is retrieved successfully, the details are shown in a tabular form.

Click the `urn:scsn:ids:Plugfest-2022-Q2-Provider:resources` to show the available resources.

Click on the lock icon under _Actions_ to go the next step directly.

![](screenshots/self-description-detail.png)

### 2. Perform the contract negotiation process to establish a contract agreement

If you've clicked the lock icon in the previous step, the values in the form are filled in automatically. Otherwise, navigate to http://localhost:31000/#/artifacts/consumer and the following values can be used:
- _Connector ID_: `urn:scsn:ids:connectors:Plugfest-2022-Q2-Provider`
- _Agent ID_: `urn:scsn:ids:participants:Plugfest-Demo`
- _AccessURL_: `https://plugfest.beta.ids.smart-connected.nl/router/artifacts/urn:scsn:ids:connectors:Plugfest-2022-Q2-Provider:resources:c2212225-a86e-4438-abf3-a6758e0ce689`
- _Contract Offer_:  
  ```json
  {
    "@type": "ids:ContractOffer",
    "@id": "https://w3id.org/idsa/autogen/contractOffer/4a2f0af5-a96f-45ec-b5c8-ba714d231346",
    "ids:permission": [
        {
        "@type": "ids:Permission",
        "@id": "https://w3id.org/idsa/autogen/permission/01b269fe-5558-47b2-b425-6e7a7528ac32",
        "ids:target": {
            "@id": "urn:scsn:ids:connectors:Plugfest-2022-Q2-Provider:artifacts:9ff5d13e-bab3-4660-bedc-efc01dfaeaad"
        },
        "ids:action": [
            {
            "@id": "https://w3id.org/idsa/code/READ"
            },
            {
            "@id": "https://w3id.org/idsa/code/USE"
            }
        ]
        }
    ],
    "ids:contractStart": {
        "@value": "2021-12-31T23:00:00.000Z",
        "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
    },
    "ids:contractEnd": {
        "@value": "2022-12-30T23:00:00.000Z",
        "@type": "http://www.w3.org/2001/XMLSchema#dateTimeStamp"
    }
  }
  ```

Click `Request Contract` to perform the contract negotiation process.

![](screenshots/contract-negotiation.png)

If the process is successfully completed, the _Transfer Contract_ field in the _Request Artifact_ form should be filled with an URL similar to `https://w3id.org/idsa/autogen/contractAgreement/00000000-0000-0000-0000-000000000000`

### 3. Retrieve the artifact

If you've just executed the contract negotiation process, the _Request Artifact_ is filled in automatically. Otherwise, navigate to http://localhost:31000/#/artifacts/consumer and use the following values:
- _Artifact ID_: `urn:scsn:ids:connectors:Plugfest-2022-Q2-Provider:artifacts:9ff5d13e-bab3-4660-bedc-efc01dfaeaad`
- _Connector ID_: `urn:scsn:ids:connectors:Plugfest-2022-Q2-Provider`
- _Agent ID_: `urn:scsn:ids:participants:Plugfest-Demo`
- _AccessURL_: `https://plugfest.beta.ids.smart-connected.nl/router/artifacts/urn:scsn:ids:connectors:Plugfest-2022-Q2-Provider:resources:c2212225-a86e-4438-abf3-a6758e0ce689`
- _Transfer Contract_: navigate to http://localhost:31000/#/pef/pap and copy the _Agreement ID_

Click `Request Artifact` to actually download the artifact to your local system.

![](screenshots/artifact-request.png)

## Providing an artifact

> _Requires public deployment of the TSG Core Container_

To provide an artifact that can be retrieved by other connectors only a single form is required to be filled in.

1. Navigate to https://domain.name/ui/#/artifacts/provider
2. Create a _Title_ and _Description_ for your artifact
3. Select the file you want to provide
4. Leave the _Artifact ID_ empty
5. Click on _Generic read access_ to generate a contract offer that allows read access to consumers that do perform the contract negotiation process
6. Contact Maarten Kollenstart to test whether consuming the artifact works. Include the following details:
    * Access URL your connector is deployed on
    * IDS Identifier (if changed)
